import iconSrc from "./saveIcon.png";
export default function SaveButton(props) {
    return (
        <button {...props}>
            <img alt="" className="inline-block" src={iconSrc}></img>
            <span className="align-middle mx-1">Save</span>
        </button>
    );
}
