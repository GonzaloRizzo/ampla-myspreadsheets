import { nanoid } from "nanoid";
import { useState } from "react";
import SaveButton from "./SaveButton";
import Spreadsheet from "./Spreadsheet";
import Window from "./Window";

function App() {
    const [fileName, setFileName] = useState(() => {
        const params = new URLSearchParams(window.location.search);
        return params.get("id");
    });

    const handleSave = () => {
        if (!fileName) {
            const id = nanoid();
            window.history.pushState("", "", `?id=${id}`);
            setFileName(id);
        }
    };

    return (
        <Window title={"My Spreadsheets" + (fileName ? ` - ${fileName}` : "")}>
            <div className="flex my-1">
                <SaveButton onClick={handleSave} />
            </div>
            <div className="overflow-auto max-h-[90vh]">
                <Spreadsheet rows={100} cols={30} fileName={fileName} />
            </div>
        </Window>
    );
}

export default App;
