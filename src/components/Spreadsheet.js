import { useEffect, useState } from "react";
import { colIndexToLabel, rowIndexToLabel } from "../helpers/matrixUtils";
import useMatrix, { initMatrix } from "../hooks/useMatrix";
import { getLocalData, setLocalData } from "../helpers/localStorage";

function Spreadsheet({ rows, cols, fileName }) {
    const [matrix, { setMatrix, setCell, calculateCell }] = useMatrix();
    const [selectedCell, setSelectedCell] = useState();

    useEffect(() => {
        if (matrix) {
            if (fileName) {
                setLocalData(`file:${fileName}`, matrix);
            }
        } else {
            const cachedMatrix = fileName && getLocalData(`file:${fileName}`);

            setMatrix(cachedMatrix || initMatrix(rows, cols));
        }
    }, [fileName, matrix, rows, cols, setMatrix]);

    if (!matrix) {
        return null;
    }

    const tableHeader = (
        <tr>
            <HeaderCell />
            {matrix[0].map((_, colIndex) => (
                <HeaderCell key={colIndex} value={colIndexToLabel(colIndex)} />
            ))}
        </tr>
    );

    const tableBody = matrix.map((row, rowIndex) => (
        <tr key={rowIndex}>
            <HeaderCell value={rowIndexToLabel(rowIndex)} />
            {row.map((cell, colIndex) => (
                <Cell
                    getDisplayValue={() =>
                        calculateCell({ row: rowIndex, col: colIndex })
                    }
                    key={colIndex}
                    value={cell}
                    onClick={() =>
                        setSelectedCell({ row: rowIndex, col: colIndex })
                    }
                    onBlur={() => setSelectedCell(null)}
                    onChange={(value) =>
                        setCell({ row: rowIndex, col: colIndex }, value)
                    }
                    isSelected={
                        selectedCell?.row === rowIndex &&
                        selectedCell?.col === colIndex
                    }
                />
            ))}
        </tr>
    ));

    return (
        <table className="w-full h-full">
            <thead>{tableHeader}</thead>
            <tbody>{tableBody}</tbody>
        </table>
    );
}

function HeaderCell({ value }) {
    return (
        <th className="h-full p-0 ">
            <button className="h-full w-full p-0 flex place-content-center place-items-center">
                <span>{value}</span>
            </button>
        </th>
    );
}

function Cell({
    value,
    getDisplayValue,
    isSelected = false,
    onClick,
    onChange,
    onBlur,
}) {
    const [cachedValue, setCachedValue] = useState(null);

    const handleBlur = () => {
        onBlur();

        if (cachedValue !== null) {
            onChange(cachedValue);
            setCachedValue(null);
        }
    };

    const handleKeyDown = (e) => {
        if (e.key === "Enter") {
            handleBlur();
        }
    };

    return (
        <td
            className={`
                border p-0 bg-white hover:bg-slate-300
                ${isSelected && "outline outline-blue-400"}
            `}
            onClick={onClick}
        >
            {isSelected ? (
                <input
                    className="w-full h-full outline-none border-none px-1"
                    onBlur={handleBlur}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => setCachedValue(e.target.value)}
                    defaultValue={value || cachedValue || ""}
                    autoFocus
                />
            ) : (
                <span className="p-1">
                    {typeof getDisplayValue === "function"
                        ? getDisplayValue()
                        : value}
                </span>
            )}
        </td>
    );
}

export default Spreadsheet;
