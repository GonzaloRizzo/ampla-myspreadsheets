import React from "react";
import { parsePosition } from "../helpers/matrixUtils";

export function initMatrix(rows, cols) {
    const data = Array(rows);
    for (let i = 0; i < rows; i++) {
        data[i] = Array(cols).fill("");
    }
    return data;
}

export default function useMatrix() {
    const [matrix, setMatrix] = React.useState();

    const setCell = (position, value) => {
        setMatrix((oldMatrix) => {
            const newMatrix = oldMatrix.slice();

            const { row, col } = parsePosition(position);

            newMatrix[row] = newMatrix[row].slice();
            newMatrix[row][col] = value;

            return newMatrix;
        });
    };

    const calculateCell = (position) => {
        const stack = [];

        const calculate = (position) => {
            const { row, col } = parsePosition(position);
            const rawValue = matrix[row][col];

            if (!rawValue.startsWith("=")) {
                return rawValue;
            }

            const formula = rawValue.slice(1);

            if (stack.includes(formula)) {
                return "#ERR";
            }

            stack.push(formula);

            let result = calculate(formula);

            return result;
        };

        try {
            return calculate(position);
        } catch (e) {
            return "#ERR";
        }
    };

    return [matrix, { setMatrix, setCell, calculateCell }];
}
