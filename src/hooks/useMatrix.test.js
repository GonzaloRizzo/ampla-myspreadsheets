import { renderHook, act } from "@testing-library/react-hooks";
import useMatrix, { initMatrix } from "./useMatrix";
import { parseLabel } from "../helpers/matrixUtils";

describe("useMatrix()", () => {
    describe("setCell(position, setCachedValue)", () => {
        test("it supports string position", () => {
            const { result } = renderHook(() => useMatrix());

            act(() => {
                const { setCell, setMatrix } = result.current[1];

                setMatrix(initMatrix(2, 2));
                setCell("B2", "ok");
            });

            expect(result.current[0][1][1]).toBe("ok");
        });

        test("it supports object position", () => {
            const { result } = renderHook(() => useMatrix());

            act(() => {
                const { setCell, setMatrix } = result.current[1];

                setMatrix(initMatrix(2, 2));
                setCell({ col: 1, row: 1 }, "ok");
            });

            expect(result.current[0][1][1]).toBe("ok");
        });
    });

    describe("calculateCell(position)", () => {
        test("it supports references", () => {
            const { result } = renderHook(() => useMatrix());

            act(() => {
                const { setCell, setMatrix } = result.current[1];

                setMatrix(initMatrix(2, 2));

                setCell("A1", "=B2");
                setCell("B1", "ok");
                setCell("A2", "=A1");
                setCell("B2", "=B1");
            });

            expect(result.current[1].calculateCell(parseLabel("A2"))).toBe(
                "ok"
            );
        });

        test("it returns #ERR for ciclic references", () => {
            const { result } = renderHook(() => useMatrix());

            act(() => {
                const { setCell, setMatrix } = result.current[1];

                setMatrix(initMatrix(3, 1));

                setCell("A1", "=A2");
                setCell("A2", "=A3");
                setCell("A3", "=A1");
            });

            expect(result.current[1].calculateCell(parseLabel("A1"))).toBe(
                "#ERR"
            );
        });
    });
});
