export function getLocalData(key) {
    let data;
    try {
        data = JSON.parse(localStorage.getItem(key));
    } catch {}

    return data;
}

export function setLocalData(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}
