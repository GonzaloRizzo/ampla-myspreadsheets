const LETTER_START = 65;
const LETTER_SPACE = 26;
const LABEL_PATTERN = /([sA-Z]+)([0-9]+)/;

export function rowLabelToIndex(rowLabel) {
    return parseInt(rowLabel) - 1;
}

export function rowIndexToLabel(row) {
    let result = "";

    if (row >= 0) {
        result = `${row + 1}`;
    }

    return result;
}

export function colIndexToLabel(column) {
    let result = "";

    while (column >= 0) {
        result = String.fromCharCode((column % LETTER_SPACE) + LETTER_START) + result;
        column = Math.floor(column / LETTER_SPACE) - 1;
    }

    return result.toUpperCase();
}

export function colLabelToIndex(colLabel) {
    if (!typeof colLabel === "string") {
        return null;
    }

    const label = colLabel.toUpperCase();

    let result = 0;

    for (let i = 0, j = label.length - 1; i < label.length; i += 1, j -= 1) {
        const power = Math.pow(LETTER_SPACE, i);
        const scalar = label[j].codePointAt(0) - LETTER_START + 1;

        result += power * scalar;
    }

    return result - 1;
}

export function parseLabel(label) {
    const { 1: colLabel, 2: rowLabel } = label
        .toUpperCase()
        .match(LABEL_PATTERN);

    return {
        col: colLabelToIndex(colLabel),
        row: rowLabelToIndex(rowLabel),
    };
}

export function parsePosition(position) {
    return typeof position === "string" ? parseLabel(position) : position;
}
