import { colIndexToLabel, colLabelToIndex, parseLabel, rowIndexToLabel, rowLabelToIndex } from "./matrixUtils";

describe("rowLabelToIndex", ()=>{
    test("expect 1 to be 0", ()=>{
        expect(rowLabelToIndex("1")).toBe(0)
    })
    test("expect 2 to be 1", ()=>{
        expect(rowLabelToIndex("2")).toBe(1)
    })
})
describe("rowIndexToLabel", ()=>{
    test("expect 0 to be 1", ()=>{
        expect(rowIndexToLabel(0)).toBe("1")
    })
    test("expect 1 to be 2", ()=>{
        expect(rowIndexToLabel(1)).toBe("2")
    })
})


describe("colIndexToLabel", () => {
    test("expect 0 to be A", () => {
        expect(colIndexToLabel(0)).toBe("A");
    });
    test("expect 25 to be Z", () => {
        expect(colIndexToLabel(25)).toBe("Z");
    });
    test("expect 26 to be AA", () => {
        expect(colIndexToLabel(26)).toBe("AA");
    });
    test("expect 51 to be AZ", () => {
        expect(colIndexToLabel(51)).toBe("AZ");
    });
    test("expect 1352 to be AZA", () => {
        expect(colIndexToLabel(1352)).toBe("AZA");
    });
    test("expect 3562 to be EGA", () => {
        expect(colIndexToLabel(3562)).toBe("EGA");
    });
});

describe("colLabelToIndex", () => {
    test("expect A to be 0", () => {
        expect(colLabelToIndex("A")).toBe(0);
    });
    test("expect Z to be 25", () => {
        expect(colLabelToIndex("Z")).toBe(25);
    });
    test("expect AA to be 26", () => {
        expect(colLabelToIndex("AA")).toBe(26);
    });
    test("expect AZ to be 51", () => {
        expect(colLabelToIndex("AZ")).toBe(51);
    });
    test("expect AZA to be 1352", () => {
        expect(colLabelToIndex("AZA")).toBe(1352);
    });
    test("expect EGA to be 3562", () => {
        expect(colLabelToIndex("EGA")).toBe(3562);
    });
});

describe("parseLabel", () => {
    test("expect L31 to be 11,30", () => {
        expect(parseLabel("L31")).toStrictEqual({ col: 11, row: 30 });
    });

    test("expect AZ99 to be 51,98", () => {
        expect(parseLabel("AZ99")).toStrictEqual({ col: 51, row: 98 });
    });
});
