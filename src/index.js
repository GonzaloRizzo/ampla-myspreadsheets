import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "98.css";
import App from "./components/App";

ReactDOM.render(
    <React.StrictMode>
        <div className="p-4 h-screen bg-teal-600" data-testid="main">
            <App />
        </div>
    </React.StrictMode>,
    document.getElementById("root")
);
